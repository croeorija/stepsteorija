# normalization study tonight wit habishk 
![](images/2023-04-25-15-15-51.png)
![](images/2023-04-25-16-51-57.png)

definicija  
![](images/2023-04-25-15-16-22.png)

data redundancy definition  
![](images/2023-04-25-15-17-08.png)

issues due to redundancy  
![](images/2023-04-25-15-18-05.png)

insertion anomaly  
![](images/2023-04-25-15-18-27.png)

deletion anomaly  
![](images/2023-04-25-15-19-42.png)

data redundancy  
![](images/2023-04-25-15-20-10.png)

normalization - student table => student + branch   
![](images/2023-04-25-15-20-53.png)

1st normal form  
![](images/2023-04-25-15-35-36.png)
![](images/2023-04-25-15-35-12.png)

1st nf rules  
![](images/2023-04-25-15-36-14.png)

rule 1 - each column atomic values  
![](images/2023-04-25-15-36-29.png)

rule 2 - each column same type   
![](images/2023-04-25-15-36-49.png)

rule 3 - each column unique name   
![](images/2023-04-25-15-37-26.png)

rule 4 - order doesn't matter  
![](images/2023-04-25-15-37-44.png)

