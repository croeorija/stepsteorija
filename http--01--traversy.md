# traversy http
![](images/2023-05-01-04-18-17.png)

http header fields  
![](images/2023-05-01-04-18-43.png)
![](images/2023-05-01-04-18-52.png)
![](images/2023-05-01-04-19-02.png)

http status codes  
![](images/2023-05-01-04-20-43.png)
![](images/2023-05-01-04-20-58.png)

http/2  
![](images/2023-05-01-04-21-56.png)

http/2 multiplexing  
![](images/2023-05-01-04-22-23.png)